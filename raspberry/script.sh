#!/bin/bash

sudo apt-get install -y libncurses5-dev gcc make git exuberant-ctags bc libssl-dev sshpass

if test "x$RASPADDR" == "x"; then
  echo "Setup RASPADDR env variable to raspberry IP address"
  echo "Example: export RASPADDR=192.168.1.201"
  exit 1
fi

if test "x$PASS" == "x"; then
  echo "Setup PASS env variable to raspberry password for user pi"
  echo "Example: export PASS=raspberry"
  exit 2
fi

# Toolchain name depends on HOST architecture
MACH=`uname -m`
if test "x$MACH" == "xx86_64"; then
  TOOLCHAIN=gcc-linaro-arm-linux-gnueabihf-raspbian-x64
else
  TOOLCHAIN=gcc-linaro-arm-linux-gnueabihf-raspbian
fi

mkdir -p raspberry
cd raspberry

# Create source files for Cross compiling
echo "export PASS=$PASS" > raspi.comm.rc
echo "export RASPADDR=$RASPADDR" >> raspi.comm.rc
echo "export PATH=/opt/$TOOLCHAIN/bin:\$PATH" >> raspi.comm.rc
echo "export RASPI_REPOS_DIR=$PWD" >> raspi.comm.rc
echo 'export INSTALL_MOD_PATH=${RASPI_REPOS_DIR}/modules' >> raspi.comm.rc
echo 'export KERNEL_SRC=${RASPI_REPOS_DIR}/linux' >> raspi.comm.rc
echo 'export CCPREFIX=arm-linux-gnueabihf-' >> raspi.comm.rc
echo '' >> raspi.comm.rc
echo 'alias rpi-make="ARCH=arm CROSS_COMPILE=$CCPREFIX make"' >> raspi.comm.rc
echo 'alias rpi-config="rpi-make $DEFCONFIG"' >> raspi.comm.rc
echo 'alias rpi-driver-make="rpi-make -C ${KERNEL_SRC}"' >> raspi.comm.rc
echo 'alias rpi-ssh="sshpass -p ${PASS} ssh pi@$RASPADDR"' >> raspi.comm.rc
echo 'rpi-cp () { sshpass -p ${PASS} scp "$@" pi@"$RASPADDR":; }' >> raspi.comm.rc
echo 'rpi-bring () { sshpass -p ${PASS} scp pi@"$RASPADDR":$@; }' >> raspi.comm.rc

echo 'export KERNEL=kernel' > raspi.rc
echo 'export DEFCONFIG=bcmrpi_defconfig' >> raspi.rc
echo '' >> raspi.rc
echo 'source raspi.comm.rc' >> raspi.rc

echo 'export KERNEL=kernel7' > raspi2.rc
echo 'export DEFCONFIG=bcm2708_defconfig' >> raspi2.rc
echo '' >> raspi2.rc
echo 'source raspi.comm.rc' >> raspi2.rc

. raspi.rc

RES=`rpi-ssh 'pwd'`
if test "x$RES" != "x/home/pi"; then
  echo "No connection to raspberry at $RASPADDR"
  echo "RES: $RES"
  exit 3
fi

# Check for wiringPi at raspberry. Install if not present
RES=`rpi-ssh 'ls /usr/local/lib/libwiringPi.so'`
if test "x$RES" == "x"; then
  rpi-ssh 'git clone git://git.drogon.net/wiringPi && cd wiringPi && ./build'
  RES=`rpi-ssh 'ls /usr/local/lib/libwiringPi.so'`
  if test "x$RES" == "x"; then
    echo "Error installing wiringPi at raspberry. Do it manually"
    exit 4
  fi
else
  echo "wiringPi already installed in raspi"
fi

# Bring files from wiringPi in raspberry to Host for cross-compiling
if ! test -d "include/wiringPi"; then
  mkdir -p include/wiringPi
  rpi-bring /usr/local/include/* include/wiringPi

  mkdir -p lib
  rpi-bring /usr/local/lib/libwiringPi*.so.* lib
  cd lib
  ln -s libwiringPiDev.so.* libwiringPiDev.so
  ln -s libwiringPi.so.* libwiringPi.so
  cd ..
else
  echo "wiringPi already copied into host"
fi

# Native or cross compilation:
# https://www.raspberrypi.org/documentation/linux/kernel/building.md
# Cross Toolchain
if ! test -d /opt/$TOOLCHAIN; then
  if ! test -d tools; then
    git clone https://github.com/raspberrypi/tools
  else
    echo "Tools repo already downloaded in HOST"
  fi
  sudo cp -r tools/arm-bcm2708/$TOOLCHAIN /opt
else
  echo "toolchain already installed in host"
fi

# Linux kernel for Raspberry
if ! test -d linux; then
  git clone --depth=1 https://github.com/raspberrypi/linux
else
  echo "Linux kernel already pulled from repo"
fi

# Virtual machine for raspberry with Linux Kernel: read repo README.md
#git clone https://github.com/adafruit/Adafruit-Pi-Kernel-o-Matic Kernel-o-Matic

# Configure kernel with default configuration for raspi
cd linux
if ! test -f "arch/arm/boot/zImage"; then
  rpi-config

  # Modify default module configuration
  # rpi-make menuconfig
  # vi .config

  # Cross-Compile kernel
  rpi-make -j8 zImage dtbs
else
  echo "Linux kernel already compiled and available"
fi

# Alternatively, copy configuration from your raspi if available
# zcat /proc/config.gz .config
# scp /boot/config-<kernel-version> .config
mkdir -p $INSTALL_MOD_PATH
rpi-driver-make -j8 modules modules_install
#rpi-driver-make modules modules_install | grep DEPMOD | awk '{print $2}'
RPI_KERNEL=`ls $INSTALL_MOD_PATH/lib/modules/`
RES=`rpi-ssh 'uname -r'`
if test "x$RPI_KERNEL" == "x"; then
  echo "Error installing linux modules on $INSTALL_MOD_PATH"
  exit 4
elif ! test "x$RPI_KERNEL" == "x$RES"; then
  sshpass -p $PASS rsync -avP $INSTALL_MOD_PATH/lib/modules/* pi@$RASPADDR:
  rpi-ssh "sudo mv $RPI_KERNEL /lib/modules"

  # Copy kernel to raspi
  rpi-cp arch/arm/boot/zImage
  rpi-ssh 'if ! test -f /boot/kernel.img.orig; then sudo cp /boot/kernel.img /boot/kernel.img.orig; fi; sudo mv /home/pi/zImage /boot/kernel.img;'
else
  echo "Kernel installed in raspi has same version than compiled"
fi


