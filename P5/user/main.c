#include <wiringPi.h>

int 
main (int argc, char** argv)
{
  wiringPiSetupGpio();

  pinMode(0, OUTPUT);

  while (1) {
    digitalWrite(0, 1);
    delay(1000);
    digitalWrite(0, 0);
    delay(1000);
  }
  return 0;
}
