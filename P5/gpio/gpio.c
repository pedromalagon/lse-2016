#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/ioport.h>

#include <asm/uaccess.h>
#include <asm/io.h>

#define DRIVER_NAME "gpio"
/* Raspberry Pi GPIO 0-31 Kernel Driver */

#define BASE_PERI_ADDR 0x20000000
#define BASE_GPIO_ADDR (BASE_PERI_ADDR + 0x200000)

/* Function declaration of gpio.c */
static int gpio_open   (struct inode *inode, struct file *filp);
static int gpio_release(struct inode *inode, struct file *filp);
static ssize_t gpio_read (struct file *filp, char *buf, size_t count, loff_t *f_pos);
static ssize_t gpio_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);
static void gpio_exit(void);
static int gpio_init(void);

module_init(gpio_init);
module_exit(gpio_exit);

/* File access functions */
struct file_operations gpio_fops =
{ 
  read:    gpio_read,
  write:   gpio_write,
  open:    gpio_open,
  release: gpio_release
};

/* Register request status */
static int reqFlag;

/* Register addresses */
typedef struct {
  uint32_t GPFSEL[6];
  uint32_t Reserved1;
  uint32_t GPSET[2];
  uint32_t Reserved2;
  uint32_t GPCLR[2];
  uint32_t Reserved3;
  uint32_t GPLEV[2];
} GPIO_t;

GPIO_t *s_gpio = NULL;

/*
static const uint32_t GPFSELbaseaddr = 0x20200000;
static const uint32_t GPSET0addr     = 0x2020001C;
static const uint32_t GPCLR0addr     = 0x20200028;
static const uint32_t GPLEV0addr     = 0x20200034;
static const uint32_t GPPUDaddr      = 0x20200094;
static const uint32_t GPPUDCLK0addr  = 0x20200098;
*/

int gpio_init(void)
{
  //proc_entry or register_chrdev
 
  // request_region: BASE_GPIO_ADDR + 164 length (datasheet)
  /* Reserving registers */
  if (request_region(BASE_GPIO_ADDR, 164, "gpio") == NULL)
  {
    printk(KERN_ERR DIRVER_NAME " cannot reserve 0x20200000-0x202000A4\n");
    reqFlag = 1;
    gpio_exit();
    return EBUSY;
  }
  else reqFlag = 0;

  // IOREMAP
  s_gpio = (GPIO_t *)ioremap_nocache(0x20200000, sizeof(GPIO_t)); 

  printk(KERN_INFO DRIVER_NAME " module inserted\n");
  return 0;
}

void gpio_exit(void)
{
  if (s_gpio != NULL) {
    iounmap(s_gpio);
    s_gpio = NULL;
  }

  // unregister, remove proc_entry

  //release_region

  printk(KERN_INFO DRIVER_NAME " removing gpio module\n");
}

int gpio_open(struct inode *inode, struct file *filp)
{
  /* Success */
  return 0;
}

int gpio_release(struct inode *inode, struct file *filp)
{
  /* Success */
  return 0;
}

ssize_t gpio_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
  uint32_t gpio32;
  int result;

  //Use inw for reading from s_gpio fields: registers

  /* Read GPIO 0-31 level */

  /* Transfer data to user space: copy_to_user */

  /* We change the reading position as best suits */
  if (*f_pos == 0)
  {
    *f_pos += 1;
    return 1;
  }
  else return 0;
}

ssize_t gpio_write( struct file *filp, const char *buf, size_t count, loff_t *f_pos)
{
  /* Transfer data to kernel space: copy_from_user */

  /* User outw for writing */

  /* Implement a protocol for configuration */
  return 1;
}

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("GPIO device driver for RPi-B+");
MODULE_AUTHOR("DIE-UPM");
MODULE_ALIAS(DRIVER_NAME);
